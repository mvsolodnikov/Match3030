﻿using SgLib;
using SkillzSDK;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace CustonScripts
{
    public class GameSyncController : MonoBehaviour, SkillzSyncDelegate
    {
        public GameManager gameManager;
        public UnityEvent onMatchBegin;
        public UnityEvent onSkillzExit;
        public UnityEvent onMatchCompleted;
        public UnityEvent onCurrentPlayerHasLostConnection;
        public UnityEvent onCurrentPlayerHasReconected;
        public UnityEvent onCurrentPlayerHasLeftMatch;
        private bool isFirstGame = true;

        private void Awake()
        {
            GameSyncController[] gameSyncControllers = FindObjectsOfType<GameSyncController>();

            if (gameSyncControllers.Length > 1)
            {
                foreach (var syncController in gameSyncControllers)
                {
                    if (syncController != this)
                    {
                        syncController.onMatchBegin = this.onMatchBegin;
                        syncController.isFirstGame = this.isFirstGame;
                        Destroy( gameObject );
                    }
                }
            }
            
//            GameSyncController gameSyncController = FindObjectOfType<GameSyncController>();
//            if (gameSyncController)
//            {
//                gameSyncController.onMatchBegin = this.onMatchBegin;
//                Destroy( gameObject );
//            }
        }

        private void Start()
        {
            if (isFirstGame)
            {

                SkillzCrossPlatform.LaunchSkillz(this);
                ScoreManager.ScoreUpdated += score =>
                {
                    if (SkillzCrossPlatform.IsMatchInProgress())
                    {
                        SkillzCrossPlatform.UpdatePlayersCurrentScore( score );
                    }
                };
                GameManager.PlayerDied += () =>
                {
                    if (SkillzCrossPlatform.IsMatchInProgress())
                    {
                        SkillzCrossPlatform.ReportFinalScore( ScoreManager.Instance.Score );
                    }
                };
            }
            
        }

        public void SkillzAbortMatch()
        {
            SkillzCrossPlatform.AbortMatch();
        }

        public void OnMatchWillBegin( Match matchInfo )
        {
            if (isFirstGame)
            {
                isFirstGame = false;
                gameManager.StartGame();
            }
            onMatchBegin.Invoke();
        }

        public void OnSkillzWillExit()
        {
            //SkillzCrossPlatform.LaunchSkillz( this );
            Application.Quit();
            //onSkillzExit.Invoke();
        }

        public void OnMatchCompleted()
        {
            onMatchCompleted.Invoke();
        }

        public void OnDidReceiveData( byte[] value )
        {
            
        }

        public void OnCurrentPlayerHasLostConnection()
        {
            onCurrentPlayerHasLostConnection.Invoke();
        }

        public void OnCurrentPlayerHasReconnected()
        {
            onCurrentPlayerHasReconected.Invoke();
        }

        public void OnCurrentPlayerHasLeftMatch()
        {
            onCurrentPlayerHasLeftMatch.Invoke();
        }

        public void OnOpponentHasLostConnection( ulong playerId )
        {
            throw new System.NotImplementedException();
        }

        public void OnOpponentHasReconnected( ulong playerId )
        {
            throw new System.NotImplementedException();
        }

        public void OnOpponentHasLeftMatch( ulong playerId )
        {
            throw new System.NotImplementedException();
        }
    }
}