﻿namespace CustonScripts
{
    public interface ICommand
    {
        void Execute();
        void Undo();
    }
}