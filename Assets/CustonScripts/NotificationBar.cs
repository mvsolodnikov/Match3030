﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace CustonScripts
{
	public class NotificationBar : MonoBehaviour
	{

		public  Text      notificationText;
		private Coroutine showerCoroutine;

		public void ShowNotification( string text, float countdown )
		{
			if (showerCoroutine != null)
			{
				StopCoroutine( showerCoroutine );
			}

			showerCoroutine = StartCoroutine( NotificationShower( text, countdown ) );
		}

		public void ShowNotification( string text )
		{
			notificationText.text = text;
			gameObject.SetActive( true );
		}

		public void Clear()
		{
			if (showerCoroutine != null)
			{
				StopCoroutine( showerCoroutine );
			}

			notificationText.text = "";
			gameObject.SetActive( false );
		}

		private IEnumerator NotificationShower( string text, float countdown )
		{
			notificationText.text = text;
			gameObject.SetActive( true );
			yield return new WaitForSeconds( countdown );
			gameObject.SetActive( false );
		}
	
	}
}
