﻿using System.Collections;
using System.Collections.Generic;
using SgLib;
using UnityEngine;
using UnityEngine.UI;

namespace CustonScripts
{
    public class Timer : MonoBehaviour
    {
        public DirtyHackToSkillz dirtyHackToSkillz;
        public Text        timerText;
        public NotificationBar notificationBar;
        public List<ICommand> commandsAfterEndsOfTimer = new List<ICommand>();

        public bool isPaused = false;
        private Coroutine timeCoroutine;

        private void Awake()
        {
            commandsAfterEndsOfTimer.Add( new FinishGameCommand( dirtyHackToSkillz ) );
            //timeCoroutine = StartCoroutine( TimeCounter() );
            //timeCoroutine = StartCoroutine( Countdown( 70 ) );
        }

        private void Start()
        {
            StartTimer( 180 );
        }

        public void StartTimer( int secoundsRemaind )
        {
            ResumeTimer();
            gameObject.SetActive( true );
            if (timeCoroutine != null)
            {
                StopCoroutine( timeCoroutine );
            }

            timeCoroutine = StartCoroutine( Countdown( secoundsRemaind ) );
        }


        public void PauseTimer()
        {
            isPaused = true;
        }

        public void ResumeTimer()
        {
            isPaused = false;
        }


        public void StopTimer()
        {
            if (timeCoroutine == null)
            {
                return;
            }
            StopCoroutine( timeCoroutine );
            timeCoroutine = null;
            gameObject.SetActive( false );
        }



        IEnumerator Countdown( int seconds )
        {
            int duration = seconds;

            while (duration > 0)
            {
                if (!isPaused)
                {
                    duration--;
                    UpdateRemaindCounterText( duration );

                    if (duration == 59) notificationBar.ShowNotification( "<color=orange>LAST MINUTE TO GO!</color>" );

                    if (duration == 0 && commandsAfterEndsOfTimer != null && commandsAfterEndsOfTimer.Count > 0)
                    {
                        notificationBar.Clear();
                        foreach (var command in commandsAfterEndsOfTimer) command.Execute();

                        gameObject.SetActive( false );
                    }
                }

                yield return new WaitForSeconds( 1 );
            }

        }

        private void UpdateRemaindCounterText( int secoundsRemaind )
        {
            int remMinutes = secoundsRemaind / 60;
            int remSecounds = secoundsRemaind % 60;

            if (remMinutes > 0)
            {
                if (remSecounds >= 10)
                {
                    timerText.text = string.Format( "<color=black>{0}:{1}</color>", remMinutes, remSecounds );
                }
                else
                {
                    timerText.text = string.Format( "<color=black>{0}:0{1}</color>", remMinutes, remSecounds );
                }
            }
            else
            {
                if (remSecounds >= 10)
                {
                    timerText.text = string.Format( "<color=red>{0}:{1}</color>", remMinutes, remSecounds );
                }
                else
                {
                    timerText.text = string.Format( "<color=red>{0}:0{1}</color>", remMinutes, remSecounds );
                }
            }
            
        }
        
        
        

    }

    public class FinishGameCommand : ICommand
    {

        private readonly DirtyHackToSkillz skillzHack;
        
        public FinishGameCommand( DirtyHackToSkillz skillzHack )
        {
            this.skillzHack = skillzHack;
        }
        
        public void Execute()
        {
            Debug.Log( "Gameover!" );
            skillzHack.AbortMatch();
        }

        public void Undo()
        {
        }
    }

    
}