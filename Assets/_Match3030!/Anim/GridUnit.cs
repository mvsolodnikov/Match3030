﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridUnit : MonoBehaviour {
    public static event System.Action<GridUnit> aPentominoHasSetInPlace = delegate { };
    public Vector2 gridUnitIndex = new Vector2(-1, -1);
    public static string gridUnitTag;
    public PentominoUnit.pentominoColor pentominoUnitColor = 0;
    public GameObject pentominoUnitAtThisPosition = null;
	// Use this for initialization
	void Start () {
        gridUnitTag = gameObject.tag;
        PentominoUnit.PentominoUnitSnapedToGrid += OnPentominoSnapToGrid;
	}
    private void OnDestroy()
    {
        PentominoUnit.PentominoUnitSnapedToGrid -= OnPentominoSnapToGrid;

    }

    private void OnPentominoSnapToGrid(PentominoUnit obj, GameObject arg2)
    {
        if (arg2.Equals(gameObject))
        {
            pentominoUnitColor = obj.pentominoUnitColor;
            pentominoUnitAtThisPosition = obj.gameObject;
            aPentominoHasSetInPlace(this);
        }
    }
}
