﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseDialog : MonoBehaviour {

    private void OnEnable()
    {
        GetComponentInChildren<Animator>().SetTrigger("Pause");
    }
}
