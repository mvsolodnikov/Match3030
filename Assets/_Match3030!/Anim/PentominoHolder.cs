﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SgLib;
public class PentominoHolder : MonoBehaviour {
    public static event System.Action<GameObject> PentominoHolderMoved = delegate { };
    public static event System.Action<GameObject> PentominoHolderDroped = delegate { };
    public static event System.Action<GameObject> pentominoSnapToGrid = delegate { };
    private static GameObject pentominoObjectBeingDrag = null;
    public GameObject polyominoUnitPreFab;
    public List<GameObject> pentominoUnitList = new List<GameObject>();
    public float dragThreshold = 0.2f;
    [HideInInspector]
    public Vector3 defaultPosition = Vector3.zero;
    //private int pentominoUnitLayerMask = 1 <<10;
    [HideInInspector]
    public int rotatePosition = 0;
    private bool isRotating = false;
    private void Awake()
    {
        GameManager.TouchStateChange += OnTouchStateChanged;
        
    }

    void OnEnable()
    {
        isRotating = false;
    }

    private void OnDestroy()
    {
        GameManager.TouchStateChange -= OnTouchStateChanged;
    }
    
    bool beingDrag = false;
    private void OnTouchStateChanged(GameManager.TouchState obj)
    {
        if (obj.Equals(GameManager.TouchState.Enter))
        {
            //Vector3 worldTouch = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //RaycastHit2D hit = Physics2D.Raycast(new Vector2(worldTouch.x, worldTouch.y), Vector2.zero, Mathf.Infinity, pentominoUnitLayerMask);
            //if (hit)
            //{
            //    if (hit.transform.parent)
            //    {
            //        if (hit.transform.parent.Equals(transform) && pentominoObjectBeingDrag == null)
            //        {
            //            pentominoObjectBeingDrag = gameObject;
            //        }
            //    }
            //}
            GameObject closedHolder = FindClosedpolyominoHolder();
            if (closedHolder !=null)
            {
                if (closedHolder.Equals(gameObject)&& pentominoObjectBeingDrag == null)
                {
                    pentominoObjectBeingDrag = gameObject;
                }
            }
        }
        else if (obj.Equals(GameManager.TouchState.Move)&& GameManager.Instance.deltaMousePosition.magnitude > GameManager.Instance.touchOrDragThresHold)
        {
            if (pentominoObjectBeingDrag)
            {
                if (pentominoObjectBeingDrag.transform.Equals(transform))
                {
                    beingDrag = true;
                    transform.localScale = Vector3.one;
                }
            }

        }
        else if (obj.Equals(GameManager.TouchState.Exit)&&pentominoObjectBeingDrag)
        {
            if (pentominoObjectBeingDrag.Equals(gameObject))
            {
                if (!beingDrag)
                {
                    RotatePentominoHolder();
                }
                else
                {
                    PentominoHolderDroped(gameObject);
                    CheckAndSnap();
                    ClearNearestGrid();
                    beingDrag = false;
                    transform.localScale = GameManager.Instance.polyominoHolderDefaultScale;
                }
                pentominoObjectBeingDrag = null;
            }
        }
        
    }

    private GameObject FindClosedpolyominoHolder()
    {
        float nearestDistance = -1;
        GameObject closedPentominoHolder = null;
        foreach (var polyominoHolder in pentominoBar.Instance.pentominoHolderList)
        {
            foreach (var pentominoUnit in polyominoHolder.GetComponent<PentominoHolder>().pentominoUnitList)
            {
                Vector3 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                float distance = Mathf.Sqrt(Mathf.Pow((pentominoUnit.transform.position - worldPos).x, 2) + Mathf.Pow((pentominoUnit.transform.position - worldPos).y, 2));
                if ((nearestDistance==-1||nearestDistance<distance)&&distance<GameManager.Instance.spacing)
                {
                    distance = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - pentominoUnit.transform.position).magnitude;
                    closedPentominoHolder = pentominoUnit.transform.parent.gameObject;
                }
            }
        }
        return closedPentominoHolder;
    }

    private void ClearNearestGrid()
    {
        foreach (var item in pentominoUnitList)
        {
            PentominoUnit pentoUnit = item.GetComponent<PentominoUnit>();
            if (pentoUnit.nearestGrid!=null)
            {
                pentoUnit.nearestGrid = null;
            }
        }
    }

    float rotateZAngle = 0;
    private void RotatePentominoHolder()
    {
        Vector3 newLocalRotation = transform.localEulerAngles;
        newLocalRotation.z += 90;
        //transform.localEulerAngles = newLocalRotation;// Rotate
        rotatePosition = (rotatePosition + 1) % 4;
        rotateZAngle = rotatePosition * 90;
        if (rotateZAngle>180)
        {
            rotateZAngle -= 360;
        }
        StartCoroutine(RotatePentominoHolderCoroutine());
    }


    private IEnumerator RotatePentominoHolderCoroutine()
    {
        isRotating = true;
        while (Mathf.DeltaAngle(transform.localEulerAngles.z,rotateZAngle)>0.01f)
        {
            Vector3 nextRotationLocation = transform.localEulerAngles;
            nextRotationLocation.z += GameManager.Instance.rotateSpeed*Mathf.Clamp(Mathf.Abs(Mathf.DeltaAngle(transform.localEulerAngles.z,rotateZAngle)),0,90)* Time.deltaTime;
            transform.localEulerAngles = nextRotationLocation;
            yield return null;
        }
        isRotating = false;
    }

    private void CheckAndSnap()
    {
        bool onGrid = false;
        foreach (var item in pentominoUnitList)
        {
            PentominoUnit pentoUnit = item.GetComponent<PentominoUnit>();
            if (!pentoUnit.gridUnitUnderThisPentominoUnit)
            {
                onGrid = false;
                break;
            }
            else
            {
                onGrid = true;
            }
        }

        if (onGrid)
        {
            pentominoSnapToGrid(pentominoObjectBeingDrag);
            Destroy(gameObject);
        }
        else
        {
            //transform.position = defaultPosition;// snap back to default position
            StartCoroutine(SnapBackToDefaultPosition());
        }
    }

    private IEnumerator SnapBackToDefaultPosition()
    {
        float distance = (transform.position - defaultPosition).magnitude;
        float movedDistance = 0;
        while (movedDistance< distance)
        {
            movedDistance += ((defaultPosition - transform.position) * GameManager.Instance.snapSpeed * Time.deltaTime).magnitude;
            transform.position += (defaultPosition - transform.position) * GameManager.Instance.snapSpeed*Time.deltaTime;
            yield return null;
        }
    }


    // Use this for initialization
    void Start () {
        polyominoUnitPreFab = CharacterManager.Instance.characters[CharacterManager.Instance.CurrentCharacterIndex];
        CreatePentoMino();
        defaultPosition = transform.position;
        transform.localScale = GameManager.Instance.polyominoHolderDefaultScale;
    }
    private void CreatePentoMino()
    {
        pentominoUnitList = new List<GameObject>();
        int[] polyominoInfor = Randompolyomino();
        foreach (var item in polyominoInfor)
        {
            if (item != 0)
            {
                int itemIndex = item - 1;
                int indexX = itemIndex / 5;
                int indexY = itemIndex % 5;
                GameObject pentominoUnit = Instantiate(polyominoUnitPreFab, gameObject.transform.position + new Vector3(indexY * GameManager.Instance.spacing, -indexX * GameManager.Instance.spacing, 0), Quaternion.identity, transform) as GameObject;
                pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex = new Vector2(indexX, indexY);
                PentominoUnit.pentominoColor pentominoColorTemp = 0;
                int numberOfColor = 5;
                if (GameManager.Instance.increaseNumberOfColorBaseOnPlayerScore)
                {
                    numberOfColor = Mathf.Clamp(ScoreManager.Instance.Score / GameManager.Instance.colorIncreasementStep + GameManager.Instance.numberOfStartColor + 1, GameManager.Instance.numberOfStartColor + 1, Enum.GetNames(typeof(PentominoUnit.pentominoColor)).Length + 1);
                }
                pentominoColorTemp = (PentominoUnit.pentominoColor)UnityEngine.Random.Range(1, numberOfColor);
                pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitColor = pentominoColorTemp;
                int changeColorLoopCount = 0;
                while (!CheckpolyominoNew(pentominoUnit,0))
                {
                    if ((int)pentominoColorTemp < numberOfColor -1)
                    {
                        pentominoColorTemp = pentominoColorTemp + 1;
                    }
                    else
                    {
                        pentominoColorTemp = (PentominoUnit.pentominoColor)1;
                    }
                    pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitColor = pentominoColorTemp;
                    changeColorLoopCount++;
                    if (changeColorLoopCount>100)
                    {
                        break;
                    }
                }
                pentominoUnit.transform.localScale = Vector3.zero;
                pentominoUnit.GetComponent<Animator>().SetTrigger("New");
                pentominoUnitList.Add(pentominoUnit);
            }
        }
        SetPentominoUnitAtCenter();
    }

    private int[] Randompolyomino()
    {
        int[] polyominoInfor = new int[5];
        int polyominoType = UnityEngine.Random.Range(0, GameManager.Instance.numberOfpolyominoType);
        switch (polyominoType)
        {
            case 0:
                polyominoInfor = PentominoInfor.pentominoIndexList[18];
                break;
            case 1:
                polyominoInfor = PentominoInfor.pentominoIndexList[19];
                break;
            case 2:
                polyominoInfor = PentominoInfor.pentominoIndexList[UnityEngine.Random.Range(20,22)];
                break;
            case 3:
                polyominoInfor = PentominoInfor.pentominoIndexList[UnityEngine.Random.Range(22, PentominoInfor.pentominoIndexList.Length)];
                break;
            case 4:
                polyominoInfor = PentominoInfor.pentominoIndexList[UnityEngine.Random.Range(0, 18)];
                break;
        }
        return polyominoInfor;
    }


    //private bool CheckPentomino(GameObject pentominoUnit)
    //{
    //    bool result = true;
    //    int numberOfPentominoHasTheColor = 0;
    //    foreach (var item in pentominoUnitList)
    //    {
    //        if (pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitColor == item.GetComponent<PentominoUnit>().pentominoUnitColor
    //            && (pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.x - item.GetComponent<PentominoUnit>().pentominoUnitIndex.x) >0
    //            && (pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.x - item.GetComponent<PentominoUnit>().pentominoUnitIndex.x)<GameManager.Instance.numberOfPentominoUnitStandNextToEachOthers
    //            && pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.y == item.GetComponent<PentominoUnit>().pentominoUnitIndex.y)
    //        {
    //            numberOfPentominoHasTheColor++;
    //        }
    //    }
    //    if (numberOfPentominoHasTheColor>=GameManager.Instance.numberOfPentominoUnitStandNextToEachOthers-1)
    //    {
    //        result = false;
    //        return result;
    //    }
    //    result = true;
    //    numberOfPentominoHasTheColor = 0;
    //    foreach (var item in pentominoUnitList)
    //    {
    //        if (pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitColor == item.GetComponent<PentominoUnit>().pentominoUnitColor
    //            && (item.GetComponent<PentominoUnit>().pentominoUnitIndex.x - pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.x) > 0
    //            && (item.GetComponent<PentominoUnit>().pentominoUnitIndex.x - pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.x) < GameManager.Instance.numberOfPentominoUnitStandNextToEachOthers
    //            && pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.y == item.GetComponent<PentominoUnit>().pentominoUnitIndex.y)
    //        {
    //            numberOfPentominoHasTheColor++;
    //        }
    //    }
    //    if (numberOfPentominoHasTheColor >= GameManager.Instance.numberOfPentominoUnitStandNextToEachOthers - 1)
    //    {
    //        result = false;
    //        return result;
    //    }
    //    result = true;
    //    numberOfPentominoHasTheColor = 0;
    //    foreach (var item in pentominoUnitList)
    //    {
    //        if (pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitColor == item.GetComponent<PentominoUnit>().pentominoUnitColor
    //            && (pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.y - item.GetComponent<PentominoUnit>().pentominoUnitIndex.y) > 0
    //            && (pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.y - item.GetComponent<PentominoUnit>().pentominoUnitIndex.y) < GameManager.Instance.numberOfPentominoUnitStandNextToEachOthers
    //            && pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.x == item.GetComponent<PentominoUnit>().pentominoUnitIndex.x)
    //        {
    //            numberOfPentominoHasTheColor++;
    //        }
    //    }
    //    if (numberOfPentominoHasTheColor >= GameManager.Instance.numberOfPentominoUnitStandNextToEachOthers - 1)
    //    {
    //        result = false;
    //        return result;
    //    }
    //    result = true;
    //    numberOfPentominoHasTheColor = 0;
    //    foreach (var item in pentominoUnitList)
    //    {
    //        if (pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitColor == item.GetComponent<PentominoUnit>().pentominoUnitColor
    //            && (item.GetComponent<PentominoUnit>().pentominoUnitIndex.y - pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.y) >0
    //            && (item.GetComponent<PentominoUnit>().pentominoUnitIndex.y - pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.y) < GameManager.Instance.numberOfPentominoUnitStandNextToEachOthers
    //            && pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.x == item.GetComponent<PentominoUnit>().pentominoUnitIndex.x)
    //        {
    //            numberOfPentominoHasTheColor++;
    //        }
    //    }
    //    if (numberOfPentominoHasTheColor >= GameManager.Instance.numberOfPentominoUnitStandNextToEachOthers - 1)
    //    {
    //        result = false;
    //        return result;
    //    }

    //    return result;
    //}

    List<GameObject> listpolyominoHasTheSameColor = new List<GameObject>();
    private bool CheckpolyominoNew(GameObject polyominoUnit, int sideHasBeenChecked)
    {
        bool result = true;
        if (sideHasBeenChecked == 0)
        {
            listpolyominoHasTheSameColor = new List<GameObject>();
        }

        PentominoUnit polymoniUnitBeingChecked = polyominoUnit.GetComponent<PentominoUnit>();

        if (sideHasBeenChecked != 1)
        {
            // Check gridUnit above
            int indexX = (int)polymoniUnitBeingChecked.pentominoUnitIndex.x - 1;
            int indexY = (int)polymoniUnitBeingChecked.pentominoUnitIndex.y;
            foreach (var item in pentominoUnitList)
            {
                if (((int) item.GetComponent<PentominoUnit>().pentominoUnitIndex.x == indexX) && ((int) item.GetComponent<PentominoUnit>().pentominoUnitIndex.y == indexY))
                {
                    if (!listpolyominoHasTheSameColor.Contains(item))
                    {
                        if (item.GetComponent<PentominoUnit>().pentominoUnitColor.Equals(polymoniUnitBeingChecked.pentominoUnitColor))
                        {
                            listpolyominoHasTheSameColor.Add(item);
                            CheckpolyominoNew(item, 2);
                        }
                    }
                }
            }
        }
        if (sideHasBeenChecked != 2)
        {
            // Check gridUnit below
            int indexX = (int)polymoniUnitBeingChecked.pentominoUnitIndex.x + 1;
            int indexY = (int)polymoniUnitBeingChecked.pentominoUnitIndex.y;
            foreach (var item in pentominoUnitList)
            {
                if (((int)item.GetComponent<PentominoUnit>().pentominoUnitIndex.x == indexX) && ((int)item.GetComponent<PentominoUnit>().pentominoUnitIndex.y == indexY))
                {
                    if (!listpolyominoHasTheSameColor.Contains(item))
                    {
                        if (item.GetComponent<PentominoUnit>().pentominoUnitColor.Equals(polymoniUnitBeingChecked.pentominoUnitColor))
                        {
                            listpolyominoHasTheSameColor.Add(item);
                            CheckpolyominoNew(item, 1);
                        }
                    }
                }
            }
        }
        if (sideHasBeenChecked != 3)
        {
            // Check gridUnit left
            int indexX = (int)polymoniUnitBeingChecked.pentominoUnitIndex.x;
            int indexY = (int)polymoniUnitBeingChecked.pentominoUnitIndex.y-1;
            foreach (var item in pentominoUnitList)
            {
                if (((int)item.GetComponent<PentominoUnit>().pentominoUnitIndex.x == indexX) && ((int)item.GetComponent<PentominoUnit>().pentominoUnitIndex.y == indexY))
                {
                    if (!listpolyominoHasTheSameColor.Contains(item))
                    {
                        if (item.GetComponent<PentominoUnit>().pentominoUnitColor.Equals(polymoniUnitBeingChecked.pentominoUnitColor))
                        {
                            listpolyominoHasTheSameColor.Add(item);
                            CheckpolyominoNew(item, 4);
                        }
                    }
                }
            }
        }
        if (sideHasBeenChecked != 4)
        {
            // Check gridUnit right
            int indexX = (int)polymoniUnitBeingChecked.pentominoUnitIndex.x;
            int indexY = (int)polymoniUnitBeingChecked.pentominoUnitIndex.y+1;
            foreach (var item in pentominoUnitList)
            {
                if (((int)item.GetComponent<PentominoUnit>().pentominoUnitIndex.x == indexX) && ((int)item.GetComponent<PentominoUnit>().pentominoUnitIndex.y == indexY))
                {
                    if (!listpolyominoHasTheSameColor.Contains(item))
                    {
                        if (item.GetComponent<PentominoUnit>().pentominoUnitColor.Equals(polymoniUnitBeingChecked.pentominoUnitColor))
                        {
                            listpolyominoHasTheSameColor.Add(item);
                            CheckpolyominoNew(item, 3);
                        }
                    }
                }
            }
        }

        if (sideHasBeenChecked == 0)
        {
            if (listpolyominoHasTheSameColor.Count >= GameManager.Instance.numberOfpolyominoUnitStandNextToEachOthers - 1)
            {
                result = false;
                return result;
            }
        }
        return result;
    }

    private void SetPentominoUnitAtCenter()
    {
        Vector3 centerOffset = Vector3.zero;
        foreach (var item in pentominoUnitList)
        {
            centerOffset += item.transform.localPosition;
        }
        centerOffset.x = centerOffset.x / pentominoUnitList.Count;
        centerOffset.y = centerOffset.y / pentominoUnitList.Count;
        centerOffset.z = centerOffset.z / pentominoUnitList.Count;
        foreach (var item in pentominoUnitList)
        {
            item.transform.localPosition -= centerOffset;
        }
    }

    // Update is called once per frame
    void Update () {
        BeingDrag();	
	}

    Vector3 targetPosition = Vector3.zero;
    private void BeingDrag()
    {
        if (!isRotating)
        {
            if (beingDrag)
            {
                Vector3 worldTouch = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                worldTouch.z = defaultPosition.z - 2;
                targetPosition = worldTouch + GameManager.Instance.movingOffset;
                if ((transform.position - targetPosition).magnitude > dragThreshold)
                {
                    transform.position += (targetPosition - transform.position) * Time.deltaTime * GameManager.Instance.dragSpeed;
                    PentominoHolderMoved(gameObject);
                    ClearNearestGrid();
                }
            }
            CheckAndClearShadow();
        }
    }

    void CheckAndClearShadow()
    {
        bool onGrid = false;
        foreach (var item in pentominoUnitList)
        {
            PentominoUnit pentoUnit = item.GetComponent<PentominoUnit>();
            if (!pentoUnit.gridUnitUnderThisPentominoUnit)
            {
                onGrid = false;
                break;
            }
            else
            {
                onGrid = true;
            }
        }

        if (!onGrid)
        {
            foreach (var item in pentominoUnitList)
            {
                GameObject grid;
                if (grid = item.GetComponent<PentominoUnit>().gridUnitUnderThisPentominoUnit)
                {
                    if (grid!=null)
                    {
                        grid.GetComponent<SpriteRenderer>().color = GameManager.Instance.gridDefaultColor;
                    }
                }
            }
        }
    }
}
