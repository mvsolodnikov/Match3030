﻿using System.Collections;
using System.Collections.Generic;

public static class PentominoInfor {
    public static int[][] pentominoIndexList = { new int[5]{1,6,11,16,21 },
                                         new int[5] {2,3, 6, 7,12 },
                                         new int[5] {1,2, 7, 8,12 },
                                         new int[5] {2,7,12,16,17 },
                                         new int[5] {1,6,11,16,17 },
                                         new int[5] {1,2,6,7,12 },
                                         new int[5] {1,2,6,7,11 },
                                         new int[5] {2,7,11,12,16 },
                                         new int[5] {1,6,11,12,17 },
                                         new int[5] {1,2,3,7,12 },
                                         new int[5] {1,3,6,7,8 },
                                         new int[5] {3,8,11,12,13 },
                                         new int[5] {3,7,8,11,12 },
                                         new int[5] {2,6,7,8,12 },
                                         new int[5] {2,6,7,12,17 },
                                         new int[5] {1,6,7,11,16 },
                                         new int[5] {2,3,7,11,12 },
                                         new int[5] {1,2,7,12,13 },
                                         new int[5] {1,0,0,0,0},
                                         new int[5] {1,2,0,0,0},
                                         new int[5] {1,2,3,0,0},
                                         new int[5] {1,6,7,0,0},
                                         new int[5] {1,6,11,16,0 },
                                         new int[5] {1,2,6,7,0},
                                         new int[5] {1,2,6,11,0 },
                                         new int[5] {2,6,7,11,0},
                                         new int[5] {1,2,7,12,0},
                                         new int[5] {1,6,7,12,0},
                                         new int[5] {2,6,7,8,0}

    };
}
