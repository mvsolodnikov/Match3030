﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PentominoUnit : MonoBehaviour {
    public static event System.Action<PentominoUnit,GameObject> PentominoUnitSnapedToGrid = delegate { };
    public Vector2 pentominoDropIndex = new Vector2(-1, -1);
    public Vector2 pentominoUnitIndex = new Vector2(-1, -1);
    public GameObject gridUnitUnderThisPentominoUnit = null;
    private bool dropedDown = false;
    private int gridUnitLayerMask = 1 << 11;
    public GameObject nearestGrid = null;
    public enum pentominoColor
    {
        Black = 1,
        White,
        Red,
        Green,
        Blue
    }

    public pentominoColor pentominoUnitColor = 0;

    void Start () {
        PentominoHolder.PentominoHolderMoved += OnPentominoHolderMoved;
        PentominoHolder.PentominoHolderDroped += OnPentominoHolderDroped;
        PentominoHolder.pentominoSnapToGrid += OnPentominoSnapToGrid;
        SetPentominoUnitColor();
	}

    private void SetPentominoUnitColor()
    {
        SpriteRenderer pentominoUnitSprite = GetComponent<SpriteRenderer>();
        switch (pentominoUnitColor)
        {
            case pentominoColor.Black:
                pentominoUnitSprite.color = GameManager.Instance.colorList[0];
                break;
            case pentominoColor.White:
                pentominoUnitSprite.color = GameManager.Instance.colorList[1];
                break;
            case pentominoColor.Red:
                pentominoUnitSprite.color = GameManager.Instance.colorList[2];
                break;
            case pentominoColor.Green:
                pentominoUnitSprite.color = GameManager.Instance.colorList[3];
                break;
            case pentominoColor.Blue:
                pentominoUnitSprite.color = GameManager.Instance.colorList[4];
                break;
            default:
                break;
        }
    }

    private void OnDestroy()
    {
        PentominoHolder.PentominoHolderMoved -= OnPentominoHolderMoved;
        PentominoHolder.PentominoHolderDroped -= OnPentominoHolderDroped;
        PentominoHolder.pentominoSnapToGrid -= OnPentominoSnapToGrid;
    }

    private void OnPentominoSnapToGrid(GameObject pentominoHolder)
    {
        if (pentominoHolder&&!dropedDown)
        {
            if (pentominoHolder.Equals(transform.parent.gameObject))
            {
                transform.parent = null;
                Vector3 newPentominoUnitPosition = new Vector3(gridUnitUnderThisPentominoUnit.transform.position.x, gridUnitUnderThisPentominoUnit.transform.position.y, transform.position.z + 1);
                transform.position = newPentominoUnitPosition;//snap
                PentominoUnitSnapedToGrid(this,gridUnitUnderThisPentominoUnit);
                dropedDown = true;
                //GetComponent<Animator>().SetTrigger("Snap");
            }
        }
    }

    private void OnPentominoHolderDroped(GameObject pentominoHolderParent)
    {
        if (transform.parent)
        {
            if (pentominoHolderParent.Equals(transform.parent.gameObject))
            {
                if (!dropedDown)
                {
                    //CheckIfOnAGridUnit();
                    CheckIfOnGridUnitByNearestPosition();
                    ResetShadow();
                }
            }
        }
    }

    private void OnPentominoHolderMoved(GameObject pentominoHolderParent)
    {
        if (transform.parent)
        {
            if (pentominoHolderParent.Equals(transform.parent.gameObject))
            {
                if (!dropedDown)
                {
                    //CheckIfOnAGridUnit();
                    CheckIfOnGridUnitByNearestPosition();
                }
            }
        }
    }

    private void CheckIfOnAGridUnit()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.zero, Mathf.Infinity, gridUnitLayerMask);
        if (hit)
        {
            if (hit.transform.tag.Equals(GridUnit.gridUnitTag)&&!hit.transform.GetComponent<GridUnit>().pentominoUnitAtThisPosition)
            {
                gridUnitUnderThisPentominoUnit = hit.transform.gameObject;
                pentominoDropIndex = hit.transform.GetComponent<GridUnit>().gridUnitIndex;
            }
            else
            {
                gridUnitUnderThisPentominoUnit = null;
                pentominoDropIndex = new Vector2(-1, -1);
            }
        }
        else
        {
            gridUnitUnderThisPentominoUnit = null;
            pentominoDropIndex = new Vector2(-1, -1);
        }
    }

    private void CheckIfOnGridUnitByNearestPosition()
    {
        ResetShadow();
        bool foundNearest = false;
        GameObject anchorPentominoUnit = null;
        foreach (var item in transform.parent.GetComponent<PentominoHolder>().pentominoUnitList)
        {
            if (item.GetComponent<PentominoUnit>().nearestGrid!=null)
            {
                foundNearest = true;
                anchorPentominoUnit = item;
                break;
            }
        }
        if (foundNearest)
        {
            Vector2 gridIndex = new Vector2();
            Vector3 deltaPosition = transform.position - anchorPentominoUnit.transform.position;
            Vector2 deltaIndex = new Vector2(-deltaPosition.y/GameManager.Instance.spacing, deltaPosition.x/ GameManager.Instance.spacing);
            gridIndex = anchorPentominoUnit.GetComponent<PentominoUnit>().pentominoDropIndex + deltaIndex;
            int indexX =  Mathf.RoundToInt(gridIndex.x);
            int indexY =  Mathf.RoundToInt(gridIndex.y);
            if (indexX < 0 || indexX > GameManager.Instance.gridUnitList.GetLength(0) - 1 || indexY < 0 || indexY > GameManager.Instance.gridUnitList.GetLength(1) - 1)
            {
                gridUnitUnderThisPentominoUnit = null;
                pentominoDropIndex = new Vector2(-1, -1);
            }
            else
            {
                if (GameManager.Instance.gridUnitList[indexX,indexY].GetComponent<GridUnit>().pentominoUnitAtThisPosition==null)
                {
                    gridUnitUnderThisPentominoUnit = GameManager.Instance.gridUnitList[indexX, indexY];
                    pentominoDropIndex = gridIndex;
                    AddShadowOnGrid(gridUnitUnderThisPentominoUnit);
                }
                else
                {
                    gridUnitUnderThisPentominoUnit = null;
                    pentominoDropIndex = new Vector2(-1, -1);
                }
            }
        }
        else
        {
            float nearestDistance = -1;
            foreach (var item in GameManager.Instance.gridUnitList)
            {
                float distance = Mathf.Sqrt(Mathf.Pow((item.transform.position - transform.position).x,2)+ Mathf.Pow((item.transform.position - transform.position).y, 2));
                if ((distance<nearestDistance||nearestDistance==-1)&&distance< GameManager.Instance.spacing)
                {
                    nearestDistance = distance;
                    nearestGrid = item;
                }
            }
            if (nearestGrid)
            {
                if (nearestGrid.GetComponent<GridUnit>().pentominoUnitAtThisPosition == null)
                {
                    gridUnitUnderThisPentominoUnit = nearestGrid;
                    pentominoDropIndex = nearestGrid.GetComponent<GridUnit>().gridUnitIndex;
                    AddShadowOnGrid(gridUnitUnderThisPentominoUnit);
                }
                else
                {
                    gridUnitUnderThisPentominoUnit = null;
                    pentominoDropIndex = new Vector2(-1, -1);
                    nearestGrid = null;
                }
            }
            else
            {
                gridUnitUnderThisPentominoUnit = null;
                pentominoDropIndex = new Vector2(-1, -1);
            }
            
        }
    }

    private void ResetShadow()
    {
        if (gridUnitUnderThisPentominoUnit!=null)
        {
            gridUnitUnderThisPentominoUnit.GetComponent<SpriteRenderer>().color = GameManager.Instance.gridDefaultColor;
        }
    }

    private void AddShadowOnGrid(GameObject gridUnitUnderThisPentominoUnit)
    {
        gridUnitUnderThisPentominoUnit.GetComponent<SpriteRenderer>().color = GameManager.Instance.gridShadowColor;
    }
}
