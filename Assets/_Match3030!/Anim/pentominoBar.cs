﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pentominoBar : MonoBehaviour {
    public static event System.Action BarHasBeenRefilled = delegate { };
    public static pentominoBar Instance { get; private set; }
    public List<GameObject> defaultPositionList;
    public List<GameObject> pentominoHolderList = new List<GameObject>();
    public GameObject pentominoHoderPreFab;
    
    private void OnEnable()
    {
        if (pentominoHolderList.Count==0)
        {
            foreach (var item in defaultPositionList)
            {
                GameObject pentominoHolder = Instantiate(pentominoHoderPreFab, item.transform.position + new Vector3(0, 0, -1), Quaternion.identity) as GameObject;
                pentominoHolderList.Add(pentominoHolder);
            }
        }
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(Instance.gameObject);
            Instance = this;
        }
    }

    private void Start()
    {
        PentominoHolder.pentominoSnapToGrid += OnPentominoSnapToGrid;
    }

    private void OnDestroy()
    {
        PentominoHolder.pentominoSnapToGrid -= OnPentominoSnapToGrid;
    }

    private void OnPentominoSnapToGrid(GameObject dropedDownPentominoHolder)
    {
        pentominoHolderList.Remove(dropedDownPentominoHolder);
        if (pentominoHolderList.Count==0)
        {
            reFillPentominoBar();
        }
    }

    private void reFillPentominoBar()
    {
        
        StartCoroutine(GeneratePentominoHolder());
    }

    private IEnumerator GeneratePentominoHolder()
    {
        yield return new WaitForSeconds(GameManager.Instance.refillDelayTime);
        foreach (var item in defaultPositionList)
        {
            GameObject pentominoHolder = Instantiate(pentominoHoderPreFab, item.transform.position + new Vector3(0, 0, -1), Quaternion.identity) as GameObject;
            pentominoHolderList.Add(pentominoHolder);
        }
        BarHasBeenRefilled();
    }
}
