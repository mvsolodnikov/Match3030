﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using SgLib;
using System;
using CustonScripts;
using JetBrains.Annotations;

public enum GameState
{
    Prepare,
    Playing,
    Paused,
    PreGameOver,
    GameOver
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public static event System.Action<GameState, GameState> GameStateChanged = delegate { };

    public static event System.Action PlayerDied = delegate { };

    private static bool isRestart;

    public int numberOfpolyominoUnitStandNextToEachOthers = 3;

    public static event System.Action<TouchState> TouchStateChange = delegate { };

    public GameObject gridUnitPreFab, gridPosition, polyominoHolder;
    [SerializeField]
    int numberOfColumns = 10, numberOfRows = 10;

    [HideInInspector]
    public GameObject[,] gridUnitList;

    public Vector3 polyominoHolderDefaultScale = new Vector3(0.7f, 0.7f, 0.7f);
    public float spacing = 2;
    public float snapSpeed = 8;
    public float rotateSpeed = 6;
    public float dragSpeed = 12;
    public float touchOrDragThresHold = 1;
    public Vector3 movingOffset = new Vector3(0, 3, 0);
    public float refillDelayTime = 0.3f;

    public enum TouchState
    {
        Enter,
        Move,
        Exit
    }

    public Color32[] colorList = new Color32[5];
    public Color32 gridShadowColor = Color.black;
    public Color32 gridDefaultColor = Color.white;

    [Range(2, 5)]
    public int numberOfStartColor = 2;
    public int colorIncreasementStep = 20;
    [Range(1, 5)]
    public int numberOfpolyominoType = 5;
    public bool increaseNumberOfColorBaseOnPlayerScore = false;

    public GameState GameState
    {
        get
        {
            return _gameState;
        }
        private set
        {
            if (value != _gameState)
            {
                GameState oldState = _gameState;
                _gameState = value;

                GameStateChanged(_gameState, oldState);
                
                
                if (_gameState.Equals(GameState.Playing))
                {
                    if (!oldState.Equals(GameState.Paused))
                    {
                        gridUnitList = new GameObject[numberOfColumns, numberOfRows];
                        GenerateGrid();
                    }
                    polyominoHolder.SetActive(true);
                }
                else
                {
                    polyominoHolder.SetActive(false);
                }

                if (_gameState.Equals(GameState.Prepare))
                {
                    ClearAll();
                }
            }
        }
    }

    private void ClearAll()
    {
        foreach (var item in FindObjectsOfType<GridUnit>())
        {
            Destroy(item.gameObject);
        }
        foreach (var item in FindObjectsOfType<PentominoHolder>())
        {
            Destroy(item.gameObject);
        }
        foreach (var item in FindObjectsOfType<PentominoUnit>())
        {
            Destroy(item.gameObject);
        }
    }

    public static int GameCount
    { 
        get { return _gameCount; } 
        private set { _gameCount = value; } 
    }

    private static int _gameCount = 0;

    [Header("Set the target frame rate for this game")]
    [Tooltip("Use 60 for games requiring smooth quick motion, set -1 to use platform default frame rate")]
    public int targetFrameRate = 30;

    [Header("Current game state")]
    [SerializeField]
    private GameState _gameState = GameState.Prepare;

    // List of public variable for gameplay tweaking
    [Header("Gameplay Config")]

    [Range(0f, 1f)]
    [HideInInspector]
    public float coinFrequency = 0.1f;

    // List of public variables referencing other objects
    [Header("Object References")]
    public PlayerController playerController;

    void OnEnable()
    {
        PlayerController.PlayerDied += PlayerController_PlayerDied;
        GridUnit.aPentominoHasSetInPlace += OnAPentominoHasSetInPlace;
        pentominoBar.BarHasBeenRefilled += OnBarHasBeenRefilled;
        PentominoHolder.pentominoSnapToGrid += OnPentominoHolderSnapToGrid;
    }

    void OnDisable()
    {
        PlayerController.PlayerDied -= PlayerController_PlayerDied;
        GridUnit.aPentominoHasSetInPlace -= OnAPentominoHasSetInPlace;
        pentominoBar.BarHasBeenRefilled -= OnBarHasBeenRefilled;
        PentominoHolder.pentominoSnapToGrid -= OnPentominoHolderSnapToGrid;
    }

    int numberOfMinoHaveToSetInPlace = 0;

    private void OnPentominoHolderSnapToGrid(GameObject obj)
    {
        numberOfMinoHaveToSetInPlace = obj.GetComponent<PentominoHolder>().pentominoUnitList.Count;
    }

    private void OnBarHasBeenRefilled()
    {
        StartCoroutine(Wait4AllPentominoUnit());
    }

    private IEnumerator Wait4AllPentominoUnit()
    {
        yield return new WaitForSeconds(0.5f);
        if (CheckEndGame())
        {
            PlayerController_PlayerDied();
        }
    }

    private void OnAPentominoHasSetInPlace(GridUnit obj)
    {
        numberOfPentominiUnitHasSetInPlace++;
        gridUnitHasChanged.Add(obj);
        if (numberOfPentominiUnitHasSetInPlace == numberOfMinoHaveToSetInPlace)
        {
            CheckGetPoint();
            numberOfPentominiUnitHasSetInPlace = 0;
            gridUnitHasChanged = new List<GridUnit>();
            SoundManager.Instance.PlaySound(SoundManager.Instance.tick);
        }
        
    }

    List<GridUnit> gridUnitHasChanged = new List<GridUnit>();
    int numberOfPentominiUnitHasSetInPlace = 0;


    private bool CheckEndGame()
    {
        foreach (var grid in gridUnitList)
        {
            foreach (var pentominoHolder in pentominoBar.Instance.pentominoHolderList)
            {
                for (int i = 0; i < 4; i++)
                {
                    int numberOfPentominoUnitHasPlace = 0;
                    foreach (var pentominoUnit in pentominoHolder.GetComponent<PentominoHolder>().pentominoUnitList)
                    {
                        Vector2 pentominoUnitIndexTemp = new Vector2();
                        switch (i)
                        {
                            case 0:
                                pentominoUnitIndexTemp = new Vector2(pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.x, pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.y) + grid.GetComponent<GridUnit>().gridUnitIndex;
                                break;
                            case 1:
                                pentominoUnitIndexTemp = new Vector2(-pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.y, pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.x) + grid.GetComponent<GridUnit>().gridUnitIndex;
                                break;
                            case 2:
                                pentominoUnitIndexTemp = new Vector2(-pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.x, -pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.y) + grid.GetComponent<GridUnit>().gridUnitIndex;
                                break;
                            case 3:
                                pentominoUnitIndexTemp = new Vector2(pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.y, -pentominoUnit.GetComponent<PentominoUnit>().pentominoUnitIndex.x) + grid.GetComponent<GridUnit>().gridUnitIndex;
                                break;
                        }
                        int indexX = (int)pentominoUnitIndexTemp.x;
                        int indexY = (int)pentominoUnitIndexTemp.y;
                        if (indexX < 0 || indexX > gridUnitList.GetLength(0) - 1 || indexY < 0 || indexY > gridUnitList.GetLength(1) - 1)
                        {
                            break;
                        }
                        if (gridUnitList[indexX, indexY].GetComponent<GridUnit>().pentominoUnitAtThisPosition == null)
                        {
                            numberOfPentominoUnitHasPlace++;
                        }
                    }
                    if (numberOfPentominoUnitHasPlace == pentominoHolder.GetComponent<PentominoHolder>().pentominoUnitList.Count)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    
    private void CheckGetPoint()
    {
        foreach (var item in gridUnitHasChanged)
        {
            if (item)
            {
                //CheckToClear(item);
                CheckToClearNew(item, 0);
            }
        }
        ClearPentomino();
    }

    private void ClearPentomino()
    {
        if (listToClear.Count > 0)
        {
            //ScoreManager.Instance.AddScore(listToClear.Count); //счет обновляем тута
            ScoreManager.Instance.AddScore( CalculateScore( listToClear.Count ) ); //счет обновляем тута
            SoundManager.Instance.PlaySound(SoundManager.Instance.getPoint);
            StartCoroutine(DestroyPentomino(listToClear));
        }
        listToClear = new List<GameObject>();
        if (pentominoBar.Instance.pentominoHolderList.Count < pentominoBar.Instance.defaultPositionList.Count && pentominoBar.Instance.pentominoHolderList.Count != 0)
        {
            StartCoroutine(Wait2CheckEndGame());
        }
    }


    private int CalculateScore( int clearedPentaminoCount )
    {
        switch (clearedPentaminoCount)
        {
                case 0:
                    return 0;
                case 1:
                    return 10;
                case 2:
                    return 20;
                case 3:
                    return 30;
                case 4:
                    return 33;
                case 5:
                    return 36;
                case 6:
                    return 60;
                case 7:
                    return 63;
                case 8:
                    return 66;
                case 9:
                    return 90;
                case 10:
                    return 93;
                case 11:
                    return 96;
                case 12:
                    return 120;
                case 13:
                    return 123;
                case 14:
                    return 126;
                case 15:
                    return 150;
                case 16:
                    return 153;
                case 17:
                    return 156;
                case 18:
                    return 180;
                case 19:
                    return 183;
                case 20:
                    return 186;
                case 21:
                    return 210;
                case 22:
                    return 213;
                case 23:
                    return 216;
                case 24:
                    return 240;
                default:
                    return 300;
        }
    }
    
    
    private IEnumerator Wait2CheckEndGame()
    {
        yield return new WaitForSeconds(0.5f);
        if (CheckEndGame())
        {
            PlayerController_PlayerDied();
        }
    }

    private IEnumerator DestroyPentomino(List<GameObject> list)
    {
        foreach (var item in list)
        {
            item.GetComponent<PentominoUnit>().gridUnitUnderThisPentominoUnit.GetComponent<SpriteRenderer>().color = gridDefaultColor;
            item.transform.localScale *= 0.5f;
            Destroy(item.GetComponent<PentominoUnit>());
            GameObject itemTemp = Instantiate(item) as GameObject;
            Destroy(item);
            Destroy(itemTemp, 1f);
            itemTemp.GetComponent<Animator>().SetTrigger("Destroy");
        }
        yield return new WaitForEndOfFrame();
    }

    List<GameObject> listGridsHaveBeenChecked = new List<GameObject>();

    //Check grid around this unit
    private void CheckToClearNew(GridUnit grid, int sideHasBeenChecked)
    {
        if (sideHasBeenChecked == 0)
        {
            listGridsHaveBeenChecked = new List<GameObject>();
            listGridsHaveBeenChecked.Add(grid.GetComponent<GridUnit>().pentominoUnitAtThisPosition);
        }
        GridUnit gridUnitBeingChecked = grid.GetComponent<GridUnit>();
        if (sideHasBeenChecked != 1)
        {
            // Check gridUnit above
            int indexX = (int)gridUnitBeingChecked.gridUnitIndex.x - 1;
            int indexY = (int)gridUnitBeingChecked.gridUnitIndex.y;
            if (!(indexX < 0 || indexX > gridUnitList.GetLength(0) - 1 || indexY < 0 || indexY > gridUnitList.GetLength(1) - 1))
            {
                GameObject pentominoTemp = gridUnitList[indexX, indexY].GetComponent<GridUnit>().pentominoUnitAtThisPosition;
                if (pentominoTemp && pentominoTemp.GetComponent<PentominoUnit>().pentominoUnitColor.Equals(gridUnitBeingChecked.pentominoUnitColor))
                {
                    if (!listGridsHaveBeenChecked.Contains(pentominoTemp))
                    {
                        listGridsHaveBeenChecked.Add(pentominoTemp);
                        CheckToClearNew(gridUnitList[indexX, indexY].GetComponent<GridUnit>(), 2);
                    }
                }
            }
        }
        if (sideHasBeenChecked != 2)
        {
            // Check gridUnit below
            int indexX = (int)gridUnitBeingChecked.gridUnitIndex.x + 1;
            int indexY = (int)gridUnitBeingChecked.gridUnitIndex.y;
            if (!(indexX < 0 || indexX > gridUnitList.GetLength(0) - 1 || indexY < 0 || indexY > gridUnitList.GetLength(1) - 1))
            {
                GameObject pentominoTemp = gridUnitList[indexX, indexY].GetComponent<GridUnit>().pentominoUnitAtThisPosition;
                if (pentominoTemp && pentominoTemp.GetComponent<PentominoUnit>().pentominoUnitColor.Equals(gridUnitBeingChecked.pentominoUnitColor))
                {
                    if (!listGridsHaveBeenChecked.Contains(pentominoTemp))
                    {
                        listGridsHaveBeenChecked.Add(pentominoTemp);
                        CheckToClearNew(gridUnitList[indexX, indexY].GetComponent<GridUnit>(), 1);
                    }
                }
            }
        }
        if (sideHasBeenChecked != 3)
        {
            // Check gridUnit left
            int indexX = (int)gridUnitBeingChecked.gridUnitIndex.x;
            int indexY = (int)gridUnitBeingChecked.gridUnitIndex.y - 1;
            if (!(indexX < 0 || indexX > gridUnitList.GetLength(0) - 1 || indexY < 0 || indexY > gridUnitList.GetLength(1) - 1))
            {
                GameObject pentominoTemp = gridUnitList[indexX, indexY].GetComponent<GridUnit>().pentominoUnitAtThisPosition;
                if (pentominoTemp && pentominoTemp.GetComponent<PentominoUnit>().pentominoUnitColor.Equals(gridUnitBeingChecked.pentominoUnitColor))
                {
                    if (!listGridsHaveBeenChecked.Contains(pentominoTemp))
                    {
                        listGridsHaveBeenChecked.Add(pentominoTemp);
                        CheckToClearNew(gridUnitList[indexX, indexY].GetComponent<GridUnit>(), 4);
                    }
                }
            }
        }
        if (sideHasBeenChecked != 4)
        {
            // Check gridUnit right
            int indexX = (int)gridUnitBeingChecked.gridUnitIndex.x;
            int indexY = (int)gridUnitBeingChecked.gridUnitIndex.y + 1;
            if (!(indexX < 0 || indexX > gridUnitList.GetLength(0) - 1 || indexY < 0 || indexY > gridUnitList.GetLength(1) - 1))
            {
                GameObject pentominoTemp = gridUnitList[indexX, indexY].GetComponent<GridUnit>().pentominoUnitAtThisPosition;
                if (pentominoTemp && pentominoTemp.GetComponent<PentominoUnit>().pentominoUnitColor.Equals(gridUnitBeingChecked.pentominoUnitColor))
                {
                    if (!listGridsHaveBeenChecked.Contains(pentominoTemp))
                    {
                        listGridsHaveBeenChecked.Add(pentominoTemp);
                        CheckToClearNew(gridUnitList[indexX, indexY].GetComponent<GridUnit>(), 3);
                    }
                }
            }
        }
        if (sideHasBeenChecked == 0)
        {
            if (listGridsHaveBeenChecked.Count >= numberOfpolyominoUnitStandNextToEachOthers)
            {
                AddToListToClear(listGridsHaveBeenChecked);
            }
        }
    }

    List<GameObject> listToClear = new List<GameObject>();

    private void AddToListToClear(List<GameObject> checkListTemp)
    {
        foreach (var item in checkListTemp)
        {
            if (!listToClear.Contains(item))
            {
                listToClear.Add(item);
            }
        }
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(Instance.gameObject);
            Instance = this;
        }
    }

    void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

    // Use this for initialization
    void Start()
    {
        // Initial setup
        Application.targetFrameRate = targetFrameRate;
        ScoreManager.Instance.Reset();
        //gridDefaultColor = gridUnitPreFab.GetComponent<SpriteRenderer>().color;
        PrepareGame();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameState.Equals(GameState.Playing))
        {
            CheckTouchState();
        }
 
    }

    // Listens to the event when player dies and call GameOver
    public void PlayerController_PlayerDied()
    {
        StartCoroutine(Wait2GameOver());
    }

    private IEnumerator Wait2GameOver()
    {
        yield return new WaitForSeconds(0.5f);
        GameOver();
        PlayerDied();
        yield return new WaitForSeconds(1f);
        ClearAll();
    }

    // Make initial setup and preparations before the game can be played
    public void PrepareGame()
    {
        GameState = GameState.Prepare;

        // Automatically start the game if this is a restart.
        if (isRestart)
        {
            isRestart = false;
            StartGame();
        }
    }

    // A new game official starts
    public void StartGame()
    {
        GameState = GameState.Playing;
        if (SoundManager.Instance.background != null)
        {
            SoundManager.Instance.PlayMusic(SoundManager.Instance.background);
        }

        
    }

    // Called when the player died
    public void GameOver()
    {
        if (SoundManager.Instance.background != null)
        {
            SoundManager.Instance.StopMusic();
        }

        SoundManager.Instance.PlaySound(SoundManager.Instance.gameOver);
        GameState = GameState.GameOver;
        GameCount++;

        // Add other game over actions here if necessary
    }

    // Start a new game
    public void RestartGame(float delay = 0)
    {
        isRestart = true;
        StartCoroutine(CRRestartGame(delay));
    }

    IEnumerator CRRestartGame(float delay = 0)
    {
        yield return new WaitForSeconds(delay);
        //Destroy( FindObjectOfType<GameSyncController>().gameObject ); //костыль
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void HidePlayer()
    {
        if (playerController != null)
            playerController.gameObject.SetActive(false);
    }

    public void ShowPlayer()
    {
        if (playerController != null)
            playerController.gameObject.SetActive(true);
    }


    Vector3 lastRayPos = Vector3.zero;
    public Vector3 deltaMousePosition = Vector3.zero;
    bool touching = false;

    private void CheckTouchState()
    {
        if (Input.GetButton("Fire1"))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                //touch somthing;
            }
            if (touching)
            {
                if (lastRayPos != Vector3.zero && hit.point != lastRayPos)
                {
                    deltaMousePosition = hit.point - lastRayPos;
                    lastRayPos = hit.point;
                    TouchStateChange(TouchState.Move);
                }
            }
            else
            {
                lastRayPos = hit.point;
                TouchStateChange(TouchState.Enter);
            }
            touching = true;
            
        }
        else
        {
            if (touching)
            {
                touching = false;
                TouchStateChange(TouchState.Exit);
            }
        }
    }

    private void GenerateGrid()
    {
        Vector3 centerPointOffSet = Vector3.zero;
        for (int i = 0; i < numberOfRows; i++)
        {
            for (int j = 0; j < numberOfColumns; j++)
            {
                GameObject _gridUnitObject = Instantiate(gridUnitPreFab, new Vector3(j * spacing, -i * spacing, 1) + gridPosition.transform.position, Quaternion.identity, gameObject.transform) as GameObject;
                gridUnitList[i, j] = _gridUnitObject;
                centerPointOffSet += _gridUnitObject.transform.position;
                _gridUnitObject.GetComponent<GridUnit>().gridUnitIndex = new Vector2(i, j);
                _gridUnitObject.GetComponent<SpriteRenderer>().color = gridDefaultColor;
            }
        }
        centerPointOffSet.x = centerPointOffSet.x / numberOfColumns;
        centerPointOffSet.y = centerPointOffSet.y / numberOfRows;
        centerPointOffSet.z = 0;
        SetGridAtCenter();
    }

    private void SetGridAtCenter()
    {
        float x0 = (gridUnitList[0, 0].transform.localPosition.x + gridUnitList[0, numberOfColumns - 1].transform.localPosition.x) / 2 - gridPosition.transform.position.x;
        float y0 = (gridUnitList[0, 0].transform.localPosition.y + gridUnitList[numberOfRows - 1, 0].transform.localPosition.y) / 2 - gridPosition.transform.position.y;
        float z0 = 0;
        for (int i = 0; i < numberOfRows; i++)
        {
            for (int j = 0; j < numberOfColumns; j++)
            {
                gridUnitList[i, j].transform.localPosition -= new Vector3(x0, y0, z0);
            }
        }
    }

    public void PauseGame()
    {
        GameState = GameState.Paused;
    }

    public void UnPauseGame()
    {
        GameState = GameState.Playing;
    }

}
